Để kết nối với CSDL cần thêm 2 gói (có thể thực hiện ngay khi khởi tạo project bằng Spring Initializr):
- JPA
- Connector cho CSDL tương ứng. Ví dụ như MySQL

+ Trong project này sử dụng JPA và CSDL Apache H2 
(một loại CSDL nhúng).

+ Lưu ý: H2 là cơ sở dữ liệu nhúng trong bộ nhớ nên 
mỗi khi khởi động lại chương trình thì dữ liệu sẽ 
bị mất. Nếu muốn lưu trữ dữ liệu cần sử dụng các CSDL
như MySQL, MSSQL, PostgreSQL, ...

+ Xem thêm về cách đặt tên các phương thức tìm kiếm:
https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation

