package vn.vimaru.fit.ttm.introspring.introspring.student;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

// CrudRepository<LopDuLieu, Kieu_Du_Lieu_Khoa>
public interface StudentRepository extends CrudRepository<Student, String> {
  
  // findBy<Tên trường dữ liệu>
  // Tìm kiếm tất cả các học sinh có tên tương ứng
  List<Student> findByName(String name);

  // findByNameOrMsv : tìm kiếm theo tên hoặc MSV
  List<Student> findByNameOrMsv(String name, String msv);
}