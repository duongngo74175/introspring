package vn.vimaru.fit.ttm.introspring.introspring.hello;

import org.springframework.stereotype.Service;

@Service
public class FibonacciService {
  public long fibonacci(long n) {
    if (n == 0) {
      return 0;
    }

    if (n == 1) {
      return 1;
    }

    return fibonacci(n - 1) + fibonacci(n - 2);
  }
}